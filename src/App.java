public class App {
    public static void main(String[] args) throws Exception {
        CPerson person1 = new CPerson("Huong", "Ninh Thuan");
        CPerson person2 = new CPerson("Duy", "Binh Dinh");
        System.out.println(person1);
        System.out.println(person2);
        //////////////////////////////////////
        CPerson student1 = new CStudent(person1.getName(),person1.getAddress(), "java", 2000, 500);
        CPerson student2 = new CStudent(person2.getName(), person2.getAddress(), "js", 2000, 250);
        System.out.println(student1);
        System.out.println(student2);
        //////////////////////
        CStaff staff1 = new CStaff("An", "Long An", "Phan Boi Chau ", 2000);
        CStaff staff2 = new CStaff("Huong", "Ninh Thuan", "Hoang Hoa Tham", 2000);
        System.out.println(staff1);
        System.out.println(staff2);
    } 
}
