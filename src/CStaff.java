public class CStaff extends CPerson{

    public CStaff(String name, String address) {
        super(name, address);
        //TODO Auto-generated constructor stub
    }
    private String school = "";
    private double pay = 0;
    public CStaff(String name, String address, String school, double pay) {
        super(name, address);
        this.school = school;
        this.pay = pay;
    }
    public String getSchool() {
        return school;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    public double getPay() {
        return pay;
    }
    public void setPay(double pay) {
        this.pay = pay;
    }
    @Override
    public String toString() {
        return "CStaff [pay=" + pay + ", school=" + school + "]";
    }
}
